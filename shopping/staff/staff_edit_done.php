<!DOCTYPE html>
<html>

<head>
  <meta character="UTF-8">
  <title>ろくまる農園</title>
</head>

<body>

  <?php
  try {
    $staff_code = $_POST['code'];
    $staff_name = $_POST['name'];
    $staff_pass = $_POST['pass'];

    $staff_name = htmlspecialchars($staff_name, ENT_QUOTES, 'utf-8');
    $staff_pass = htmlspecialchars($staff_pass, ENT_QUOTES, 'utf-8');

    // データベースに接続
    $dsn = 'mysql:dbname=shop;host=localhost;charset=utf8';
    $user = 'root';
    $password = '';
    $dbn = new PDO($dsn, $user, $password);
    $dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // SQL文
    $sql = 'update mst_staff set name=?, password=? where code=?';
    $stmt = $dbn->prepare($sql);
    $data[] = $staff_name;
    $data[] = $staff_pass;
    $data[] = $staff_code;
    $stmt->execute($data);

    $dbn = null;
  } catch (Exception $e) {
    print 'ただいま障害により大変ご迷惑をお掛けしております';
    exit();
  }
  ?>

  修正しました。<br />
  <br />
  <a href="staff_list.php">戻る</a>

</body>

</html>
