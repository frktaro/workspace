<!DOCTYPE html>
<html>

<head>
  <meta character="UTF-8">
  <title>ろくまる農園</title>
</head>

<body>

  <?php
  try {
    $staff_code = $_GET['staffcode'];

    // データベースに接続
    $dsn = 'mysql:dbname=shop;host=localhost;charset=utf8';
    $user = 'root';
    $password = '';
    $dbn = new PDO($dsn, $user, $password);
    $dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // SQL文
    $sql = 'select name from mst_staff where code=?';
    $stmt = $dbn->prepare($sql);
    $data[] = $staff_code;
    $stmt->execute($data);

    $rec = $stmt->fetch(PDO::FETCH_ASSOC);
    $staff_name = $rec['name'];

    $dbn = null;
  } catch (Exception $e) {
    print 'ただいま障害により大変ご迷惑をお掛けしております';
    exit();
  }
  ?>

  スタッフ修正<br />
  <br />
  スタッフコード<br />
  <?php print $staff_code; ?>
  <br />
  <br />
  <form method="post" action="staff_edit_check.php">
    <input type="hidden" name="code" value="<?php print $staff_code; ?>">
    スタッフ名 <br />
    <input type="text" name="name" style="width: 200px" value="<?php print $staff_name; ?>">
    <br />

    パスワードを入力してください。<br />
    <input type="password" name="pass" style="width: 100px"><br />
    パスワードをもう１度入力してください。<br />
    <input type="password" name="pass2" style="width: 100px"><br />
    <br />

    <input type="button" onclick="history.back()" value="戻る">
    <input type="submit" value="OK">
  </form>

</body>

</html>
