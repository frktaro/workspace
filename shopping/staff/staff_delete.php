<!DOCTYPE html>
<html>

<head>
  <meta character="UTF-8">
  <title>ろくまる農園</title>
</head>

<body>

  <?php
  try {
    $staff_code = $_GET['staffcode'];

    // データベースに接続
    $dsn = 'mysql:dbname=shop;host=localhost;charset=utf8';
    $user = 'root';
    $password = '';
    $dbn = new PDO($dsn, $user, $password);
    $dbn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // SQL文
    $sql = 'select name from mst_staff where code=?';
    $stmt = $dbn->prepare($sql);
    $data[] = $staff_code;
    $stmt->execute($data);

    $rec = $stmt->fetch(PDO::FETCH_ASSOC);
    $staff_name = $rec['name'];

    $dbn = null;
  } catch (Exception $e) {
    print 'ただいま障害により大変ご迷惑をお掛けしております';
    exit();
  }
  ?>

  スタッフ削除<br />
  <br />
  スタッフコード<br />
  <?php print $staff_code; ?>
  <br />
  スタッフ名<br />
  <?php print $staff_name; ?>
  <br />
  このスタッフを削除してよろしいですか？<br />
  <br />
  <form method="post" action="staff_delete_done.php">
    <input type="hidden" name="code" value="<?php print $staff_code ?>">
    <input type="button" onclick="history.back()" value="戻る">
    <input type="submit" value="OK">
  </form>

</body>

</html>
