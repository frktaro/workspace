<?php
// 参照
if (isset($_POST['disp']) === true) {
  if (isset($_POST['staffcode']) === false) {
    header('Location:staff_ng.php');
    exit();
  }
  $staff_code = $_POST['staffcode'];
  header('Location:staff_disp.php?staffcode=' . $staff_code);
  exit();
}
// 追加
if (isset($_POST['add']) === true) {
  header('Location:staff_add.php');
  exit();
}
// 編集
if (isset($_POST['edit']) === true) {
  if (isset($_POST['staffcode']) === false) {
    // headerの前に、先に送信されるデータ(html, cookie等)があるとエラーなるので注意
    header('Location:staff_ng.php');
    exit();
  }
  $staff_code = $_POST['staffcode'];
  header('Location:staff_edit.php?staffcode=' . $staff_code);
}
// 削除
if (isset($_POST['delete']) === true) {
  if (isset($_POST['staffcode']) === false) {
    header('Location:staff_ng.php');
    exit();
  }
  $staff_code = $_POST['staffcode'];
  header('Location:staff_delete.php?staffcode=' . $staff_code);
  exit();
}
